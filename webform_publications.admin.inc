<?php

/**
 * Implementation of hook_help().
 */
function webform_publications_help($path, $arg) {
  switch ($path) {
    case 'admin/modules#description':
      return t('Allows users to search for and email local newspapers <em><strong>Note:</strong> Requires location.module.</em>');
  }
}

/**
 * Implements hook_permission().
 */
function webform_publications_permission() {
  return array(
    'administer webform publications' => array(
      'title' => t('Administer webform publications'),
      'description' => t('Adjust settings for the Webform publications module.'),
    )
  );
}

/**
 * Implements hook_menu().
 */
function webform_publications_menu() {
  $items = array();

  // Master list of all publications. 
  $items['admin/config/content/webform_publications'] = array(
    'title' => 'Webfrom publications',
    'description' => 'Edit publication information for use in webform publication components.',
    'page callback' => 'webform_publications_admin',
    'access arguments' => array('administer webform publications'),
    'type' => MENU_NORMAL_ITEM
  );
  // Add new publication.
  $items['admin/config/content/webform_publications/add'] = array(
    'title' => 'Add publication',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('webform_publications_admin_edit_form'),
    'access arguments' => array('administer webform publications'),
    'type' => MENU_LOCAL_ACTION
  );
  // Edit existing publication.
  $items['admin/config/content/webform_publications/edit/%'] = array(
    'title' => 'Edit publication',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('webform_publications_admin_edit_form', 5),
    'access arguments' => array('administer webform publications'),
    'type' => MENU_CALLBACK
  );
  // Delete existing publication.
  $items['admin/config/content/webform_publications/delete/%/%'] = array(
    'title' => 'Delete publication',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('webform_publications_admin_delete_form', 5, 6),
    'access arguments' => array('administer webform publications'),
    'type' => MENU_CALLBACK
  );
  return $items;
}

/**
 * Page callback: Webform publications settings
 *
 * @see webform_publications_menu()
 */
function webform_publications_admin() {
  // Fetch all table records.
  $result = db_query('SELECT * FROM {webform_publications} ORDER BY title ASC');
  
  // Dump resuls into array of formatted rows.
  $rows = array();
  foreach ($result as $paper) {
    $statewide = ($paper->statewide) ? ' - statewide' : '';
    $phone = ($paper->phone != NULL ? 'p: ' . check_plain($paper->phone) : '');
    $fax = ($paper->fax != NULL ? '<br />f: ' . check_plain($paper->fax) : '');
    $rows[] = array(
      'name' => ($paper->homepage != NULL ? l($paper->title, $paper->homepage) : $paper->title) . $statewide .'<br />'. l($paper->email, $paper->email),
      'address' => check_plain($paper->address) .'<br />'. check_plain($paper->city) .', '. check_plain($paper->state) .' '. check_plain($paper->zip),
      'phone' => $phone . $fax,
      'edit' => l(t('edit'), 'admin/config/content/webform_publications/edit/'. $paper->newsid),
      'delete' => l(t('delete'), 'admin/config/content/webform_publications/delete/'. $paper->newsid . '/' . $paper->title),
    );
  }

  // Set empty text if no records.
  if (empty($rows)) {
    $rows[] = array(array('data' => t('No editors available.'), 'colspan' => '5', 'class' => 'message'));
  }

  // Setup header for table presentation.
  $header = array(t('Name'), t('Address'), t('Phone Numbers'), array('data' => t('Operations'), 'colspan' => '2'));

  // Return themed paper data in table.
  return theme('table', array(
    'header' => $header, 
    'rows' => $rows,
    'attributes' => array('width' => '100%')
  ));

}

function webform_publications_admin_edit_form($form_id, $form_state, $newsid = NULL) {

  // Set default values to NULL  if $newsid = NULL.
  If ($newsid == NULL) {
    $publication = array(
      'newsid' => NULL, 
      'title' => NULL,
      'address' => NULL,
      'city' => NULL,
      'state' => NULL,
      'zip' => NULL,
      'phone' => NULL,
      'fax' => NULL,
      'email' => NULL,
      'homepage' => NULL,
      'statewide' => NULL,
    );
  }

  // Query DB for publication based on newsid.
  $result = db_query('SELECT * FROM {webform_publications} WHERE newsid = :newsid ORDER BY title ASC', array(':newsid' => $newsid));
  
  // Count results.
  $count = count($result);
  // If one result, populate array with values from DB.
  if($count == 1) {
    foreach ($result as $row) {
       $publication = array(
        'newsid' => $row->newsid,
        'title' => $row->title,
        'address' => $row->address,
        'city' => $row->city,
        'state' => $row->state,
        'zip' => $row->zip,
        'phone' => $row->phone,
        'fax' => $row->fax,
        'email' => $row->email,
        'homepage' => $row->homepage,
        'statewide' => $row->statewide,
      );     
    }
  } 
  else {
    // Throw error if no records exist for newsid.
    if ($count < 1) {
      drupal_set_message(t('The requested publication does not exist in the database.'), 'error');
    }
    // Throw error if multiple records exist with same newsid.
    if ($count > 1) {
      drupal_set_message(t('Multiple publications exist in the database with the same unique ID.'), 'error');
    }
  }

  // Populate form with values from array.
  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#default_value' => $publication['title'],
    '#required' => TRUE,
  );
  $form['address'] = array(
    '#type' => 'textfield',
    '#title' => t('Address'),
    '#default_value' => $publication['address'],
  );
  $form['city'] = array(
    '#type' => 'textfield',
    '#title' => t('City'),
    '#default_value' => $publication['city'],
  );
  $form['state'] = array(
    '#type' => 'textfield',
    '#title' => t('Province or State'),
    '#description' => t('Use two letter abbreviation.'),
    '#default_value' => $publication['state'],
    '#size' => 2, 
    '#maxlength' => 2, 
  );
  $form['zip'] = array(
    '#type' => 'textfield',
    '#title' => t('Postal or zip code'),
    '#default_value' => $publication['zip'],
    '#required' => TRUE,
    '#maxlength' => 10, 
  );
  $form['phone'] = array(
    '#type' => 'textfield',
    '#title' => t('Phone'),
    '#default_value' => $publication['phone'],
    '#maxlength' => 15, 
  );
  $form['fax'] = array(
    '#type' => 'textfield',
    '#title' => t('Fax'),
    '#default_value' => $publication['fax'],
    '#maxlength' => 15, 
  );
  $form['email'] = array(
    '#type' => 'textfield',
    '#title' => t('E-mail'),
    '#default_value' => $publication['email'],
    '#required' => TRUE,
  );
  $form['homepage'] = array(
    '#type' => 'textfield',
    '#title' => t('Homepage'),
    '#default_value' => $publication['homepage'],
  );
  $form['statewide'] = array(
    '#type' => 'checkbox',
    '#title' => t('Regional'),
    '#description' => t('Scope of publication is regional and not restricted to a single city.'),
    '#default_value' => $publication['statewide'],
  );
  $form['#redirect'] = 'admin/config/content/webform_publications';
  $form['newsid'] = array('#type' => 'value', '#value' => $publication['newsid']);
  $form['submit'] = array('#type' => 'submit', '#value' => t('Submit'));

  return $form;
}

function webform_publications_admin_edit_form_validate($form, &$form_state) {
  //check to ensure email address is valid.
  if (!valid_email_address($form_state['values']['email'])) {
    form_set_error('email', t('The email address appears to be invalid.'));
  }
}

function webform_publications_admin_edit_form_submit($form, &$form_state) {
  if ($form_state['values']['newsid']) {
    $num_updated = db_update('webform_publications')
      ->fields(array(
        'title' => $form_state['values']['title'],
        'address' => $form_state['values']['address'],
        'city' => $form_state['values']['city'],
        'state' => $form_state['values']['state'],
        'zip' => $form_state['values']['zip'],
        'phone' => $form_state['values']['phone'],
        'fax' => $form_state['values']['fax'],
        'email' => $form_state['values']['email'],
        'homepage' => $form_state['values']['homepage'],
        'statewide' => $form_state['values']['statewide'],
      ))
      ->condition('newsid', $form_state['values']['newsid'])
      ->execute();
  }
  else {
    $newsid = db_insert('webform_publications')
      ->fields(array(
        'title' => $form_state['values']['title'],
        'address' => $form_state['values']['address'],
        'city' => $form_state['values']['city'],
        'state' => $form_state['values']['state'],
        'zip' => $form_state['values']['zip'],
        'phone' => $form_state['values']['phone'],
        'fax' => $form_state['values']['fax'],
        'email' => $form_state['values']['email'],
        'homepage' => $form_state['values']['homepage'],
        'statewide' => $form_state['values']['statewide'],
      ))
      ->execute();
  }
  $form_state['redirect'] = 'admin/config/content/webform_publications';
}

function webform_publications_admin_delete_form($form_id, $form_state, $newsid = NULL, $title = NULL) {

  $question = t('Are you sure you want to delete the publication %item?', array('%item' => $title));
  $path = 'admin/config/content/webform_publications/delete';
  $description = t('This action cannot be undone.');

  $form['newsid'] = array('#type' => 'hidden', '#value' => $newsid);
  $form['name'] = array('#type' => 'hidden', '#value' => $title);

  return confirm_form($form, $question, $path, $description, t('Delete'), t('Cancel'));
}

function webform_publications_admin_delete_form_submit($form, &$form_state) {
  $num_deleted = db_delete('webform_publications')
    ->condition('newsid', $form_state['values']['newsid'])
    ->execute();

  $t_args = array('%name' => $form_state['values']['name']);
  drupal_set_message(t('The publication %name has been deleted.', $t_args));
  watchdog('menu', 'Deleted publication %name.', $t_args, WATCHDOG_NOTICE);
  $form_state['redirect'] = 'admin/config/content/webform_publications';
}
