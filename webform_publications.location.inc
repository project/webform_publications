<?php

/**
 * Find rough latitude and longditude based on p[ostal code and country.
 */
function webform_publications_location_nearby_postalcodes_bylocation($location, $distance, $distance_unit = 'km') {
  
  $latlon = location_latlon_rough($location);

  // If we could not get lat/lon coordinates for the given location, return an empty search result set.
  if (!isset($latlon['lat']) || !isset($latlon['lon'])) {
    return array();
  }

  // If the distance parameters did not make sense, return an empty search result set.
  if (!($distance_float = _location_convert_distance_to_meters($distance, $distance_unit))) {
    return array();
  }

  $search_results = _webform_publications_location_nearby_postalcodes($latlon['lon'], $latlon['lat'], $distance_float);
  
  _webform_publications_location_format_search_result_distances($search_results, $distance_unit);
  
  return $search_results;
}

/**
 * Find nearby postal codes from latitude and longditude.
 */
function _webform_publications_location_nearby_postalcodes($lon, $lat, $distance) {
  
  $search_results = array();

  $latrange = earth_latitude_range($lon, $lat, $distance);
  $lonrange = earth_longitude_range($lon, $lat, $distance);

  // Drop zip_distances view from DB
  $drop_zip_distances = db_query('DROP VIEW IF EXISTS zip_distances');

  // Create new zip_distances view containing all zipcodes within range 
  // of submitted zip or postal code. 
  $zip_result = db_query('CREATE VIEW zip_distances as SELECT zip, city, state, country,' . earth_distance_sql($lon, $lat) . ' as distance FROM {zipcodes} WHERE latitude > :latmin AND latitude < :latmax AND longitude > :longmin AND longitude < :longmax AND ' . earth_distance_sql($lon, $lat) . ' < :distance ORDER by distance', array(':latmin' => $latrange[0], ':latmax' => $latrange[1], ':longmin' => $lonrange[0], ':longmax' => $lonrange[1], ':distance' => $distance));

  // Get contents of zip_distances view
  $result = db_query('SELECT * from {zip_distances}');

  // Add results to array
  foreach ($result as $result_row) {
    $search_results[$result_row->country . $result_row->zip] = array('city' => $result_row->city, 'province' => $result_row->state, 'distance' => $result_row->distance);
  }

  return $search_results;
}

/**
 * Round off distances in results array.
 */
function _webform_publications_location_format_search_result_distances(&$results_array, $distance_unit = 'km') {
  if ($distance_unit != 'km' && $distance_unit != 'mile') {
    $distance_unit = 'km';
  }
  
  // $conversion_factor = number to divide by to convert meters to $distance_unit.
  // At this point, $distance_unit == 'km' or 'mile' and nothing else.
  $conversion_factor = ($distance_unit == 'km') ? 1000.0 : 1609.347;
  
  foreach ($results_array as $index => $single_result) {
    $results_array[$index]['distance'] = round($single_result['distance']/$conversion_factor, 1);
  }
}

/**
 * Select Publications using recently generated view
 */
function webform_publications_locate($ziparray) {
  $options = array();
  $result = array();

  if (count($ziparray) && $ziparray != 'all') {  
    //  This query selects information from webform_publications joined with a view created in function _webform_publications_location_nearby_postalcodes.  That way, the results can be ordered by distance
    $result = db_query('(SELECT n.* from  {zip_distances} z,  {webform_publications} n WHERE n.zip = z.zip and n.statewide=0 ORDER by z.distance LIMIT 4) UNION (SELECT n.* from  {zip_distances} z,  {webform_publications} n WHERE n.zip = z.zip and n.statewide=2 ORDER by z.distance LIMIT 2)');
  } 
  else {
    $result = db_query('SELECT * from {webform_publications}');
  }
  // Format results as option array.
  foreach ($result as $publication) {
    $options[$publication->email] = $publication->title.' - '.$publication->city.', '.$publication->state;
  }

  return $options;
}

/**
 * Select All Publications and handle caching for them.
 */
function webform_publications_all_publications() {
  
  static $all_publications;
  if (!isset($all_publications)) {
    // Get all publications from cache if they exist.
    if (($cache = cache_get('webform_publications_all')) && !empty($cache->data)) {
      $all_publications = $cache->data;
    }
    else {
      $all_publications = webform_publications_locate('all');
      // Populate cache and set it to expire after 24 hours.
      cache_set('webform_publications_all', $all_publications, 'cache', time() + 86400);
    }
  }

  return $all_publications;
}
